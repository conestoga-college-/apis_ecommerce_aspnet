﻿using eCommerceCore.ApiSecurity.Interfaces;
using Microsoft.AspNetCore.Http;
using System.Threading.Tasks;

namespace eCommerceCore.ApiSecurity
{
    public class ApiSession : IApiSession
    {
        private const string CUSTOMER_ID = "customerId";
        private readonly IHttpContextAccessor _httpContextAccessor;

        public ApiSession(IHttpContextAccessor httpContextAccessor)
        {
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<int> GetIdFromSession()
        {
            await _httpContextAccessor.HttpContext.Session.LoadAsync();
            var customerId = _httpContextAccessor.HttpContext.Session.GetInt32(CUSTOMER_ID) ?? 0;

            return customerId;
        }

        public async void SetIdToSession(int customerId)
        {
            _httpContextAccessor.HttpContext.Session.SetInt32(CUSTOMER_ID, customerId);
            await _httpContextAccessor.HttpContext.Session.CommitAsync();
        }

        public async Task<bool> IsUserSignedIn()
        {
            await _httpContextAccessor.HttpContext.Session.LoadAsync();
            var customerId = _httpContextAccessor.HttpContext.Session.GetInt32(CUSTOMER_ID) ?? 0;

            return customerId > 0;
        }

        public void DestroySession()
        {
            _httpContextAccessor.HttpContext.Session.Clear();
        }
    }
}
