﻿using System.Threading.Tasks;

namespace eCommerceCore.ApiSecurity.Interfaces
{
    public interface IApiSession
    {
        Task<int> GetIdFromSession();
        void SetIdToSession(int customerId);
        Task<bool> IsUserSignedIn();
        void DestroySession();
    }
}
