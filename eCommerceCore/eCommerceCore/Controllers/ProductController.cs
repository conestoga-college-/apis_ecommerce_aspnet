﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using eCommerceCore.ApiSecurity.Interfaces;
using eCommerceCore.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace eCommerceCore.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly ILogger<LoginController> _log;

        public ProductController(AppDbContext appDbContext, ILogger<LoginController> log)
        {
            _context = appDbContext;
            _log = log;

            if (_context.Products.Count() == 0)
            {
                _context.Products.AddRange(new List<Product>
                {
                    new Product {
                    Name = "Bananas",
                    Description = "Soft, sweet and delicious, the banana is a popular choice for breakfast, snacks or " +
                    "any time of the day. The vibrant yellow peel provides natural protection while storing, and is a " +
                    "great indicator of when the fruit is ready to eat!",
                    Image = "./images/products/bananas.jpg",
                    Price = 0.15m,
                    ShippingCost = 0.05m,
                    IsActive = true
                    },
                    new Product {
                    Name = "Broccoli",
                    Description = "Broccoli is a form of a cultivated cruciferous plant whose leafy stalks and clusters " +
                    "of buds are eaten as a vegetable. It is terrific steamed as a side dish or raw in a salad.",
                    Image = "./images/products/broccoli.jpg",
                    Price = 1.40m,
                    ShippingCost = 0.50m,
                    IsActive = true
                    },
                    new Product {
                    Name = "Brussels Sprouts",
                    Description = "Enjoy Brussel sprouts roasted, steamed, boiled, or grilled. Excellent source of vitamin" +
                    " K and high in fibre. Try this vegetable for your next side dish.",
                    Image = "./images/products/brussels_sprouts.jpg",
                    Price = 3.30m,
                    ShippingCost = 1.00m,
                    IsActive = true
                    },
                    new Product {
                    Name = "Lychee",
                    Description = "The finest handpicked Lychees.",
                    Image = "./images/products/lychee.jpg",
                    Price = 1.50m,
                    ShippingCost = 0.60m,
                    IsActive = true
                    },
                    new Product {
                    Name = "Apple",
                    Description = "The mild yet sweet flavour of the Gala apple has made it a family favourite for " +
                    "snacks, salads and sauces. The vibrant red mixes in with subtle hints of green to give the apple " +
                    "its unmistakable look.",
                    Image = "./images/products/apple.jpg",
                    Price = 0.70m,
                    ShippingCost = 0.10m,
                    IsActive = true
                    },
                    new Product {
                    Name = "Orange",
                    Description = "Sweet, juicy and seedless.",
                    Image = "./images/products/orange.jpg",
                    Price = 0.87m,
                    ShippingCost = 0.10m,
                    IsActive = true
                    },
                    new Product {
                    Name = "Strawberries",
                    Description = "Sweet & succulent. Blitz with low fat yogurt and oats for a super smoothie.",
                    Image = "./images/products/strawberries.jpg",
                    Price = 2.50m,
                    ShippingCost = 0.80m,
                    IsActive = true
                    },
                    new Product {
                    Name = "Tomatoes",
                    Description = "This great Ideal tomato is perfect to eat in a sandwich because of the size of " +
                    "the product !The name says it all, it\'s a large tomato with a great taste, ideal for tomato " +
                    "lovers!",
                    Image = "./images/products/tomatoes.jpg",
                    Price = 0.47m,
                    ShippingCost = 0.09m,
                    IsActive = true
                    }
                });
                _context.SaveChanges();
            }
        }

        [HttpGet]
        public async Task<object> Get()
        {
            var response = new ResponseObject();

            try
            {
                var result = await _context.Products.ToListAsync();
                response.SetResponse(result);
                return Ok(response);
            }
            catch (Exception ex)
            {
                _log.LogError(ex, "Error in ProductController");
                response.SetMessage("Ops. We got a bad behaviour from our server. Try again later.");
                return StatusCode((int)HttpStatusCode.InternalServerError, response);
            }
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<object>> GetProduct(int id)
        {
            var response = new ResponseObject();
            
            try
            {
                if (id < 0)
                {
                    response.SetMessage("Inform a valid Product ID in order to retrieve it");

                    return BadRequest(response);
                }

                var result = await _context.Products.SingleOrDefaultAsync(p => p.Id == id);

                if (result != null)
                {
                    response.SetResponse(result);
                    return Ok(response);
                }

                else
                {
                    response.SetMessage("There is no product to be shown.");
                    return Ok(response);
                }
            }
            catch (Exception ex)
            {
                _log.LogError(ex, "Error in ProductController");
                response.SetMessage("Ops. We got a bad behaviour from our server. Try again later.");
                return StatusCode((int)HttpStatusCode.InternalServerError, response);
            }
        }
    }
}
