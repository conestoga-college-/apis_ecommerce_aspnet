﻿using eCommerceCore.ApiSecurity.Interfaces;
using eCommerceCore.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace eCommerceCore.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CommentsController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly IApiSession _session;
        private readonly ILogger<LoginController> _log;

        public CommentsController(AppDbContext appDbContext, IApiSession session, ILogger<LoginController> log)
        {
            _context = appDbContext;
            _session = session;
            _log = log;

            if (_context.Comments.Count() == 0)
            {
                _context.Comments.AddRange(new List<Comment>
                {
                    new Comment
                    {
                        ProductId = 4,
                        User = _context.Users.SingleOrDefault(u => u.Id == 2),
                        Rating = 4,
                        Text = "I love peeled garlic.",
                        Date = DateTime.Now,
                    },
                    new Comment
                    {
                        ProductId = 3,
                        User = _context.Users.SingleOrDefault(u => u.Id == 2),
                        Rating = 3,
                        Text = "My kids love it!",
                        Date = DateTime.Now.AddDays(-5),
                    },
                    new Comment
                    {
                        ProductId = 4,
                        User = _context.Users.SingleOrDefault(u => u.Id == 2),
                        Rating = 3,
                        Text = "It is always fresh!",
                        Date = DateTime.Now.AddYears(-1),
                    },
                    new Comment
                    {
                        ProductId = 2,
                        User = _context.Users.SingleOrDefault(u => u.Id == 2),
                        Rating = 5,
                        Text = "Fresh broccoli",
                        Date = DateTime.Now.AddYears(-2),
                    },
                    new Comment
                    {
                        ProductId = 2,
                        User = _context.Users.SingleOrDefault(u => u.Id == 3),
                        Rating = 5,
                        Text = "They have greate products and broccoli is always green and fresh.",
                        Date = DateTime.Now,
                    },
                    new Comment
                    {
                        ProductId = 2,
                        User = _context.Users.SingleOrDefault(u => u.Id == 1),
                        Rating = 5,
                        Text = "I used to build trees on my kids plate and they love eat it.",
                        Date = DateTime.Now.AddMonths(-1),
                    },
                    new Comment
                    {
                        ProductId = 7,
                        User = _context.Users.SingleOrDefault(u => u.Id == 3),
                        Rating = 5,
                        Text = "Did you try strwberries with condensed milk? OMG!",
                        Date = DateTime.Now.AddMonths(-4),
                    },
                    new Comment
                    {
                        ProductId = 8,
                        User = _context.Users.SingleOrDefault(u => u.Id == 3),
                        Rating = 5,
                        Text = "Use them to do a sauce. But only when they are very red",
                        CommentImages = (new List<CommentImage>
                        {
                            new CommentImage
                            {
                                CommentId = 8,
                                Image = "./images/uploads/5cecc06528dfa_9_tomatoes2222.jpg"
                            }
                        }),
                        Date = DateTime.Now.AddMonths(-1).AddHours(3),
                    },
                    new Comment
                    {
                        ProductId = 5,
                        User = _context.Users.SingleOrDefault(u => u.Id == 3),
                        Rating = 5,
                        Text = "Apples are also good to make a juice",
                        Date = DateTime.Now.AddMonths(-3).AddHours(3),
                    },
                    new Comment
                    {
                        ProductId = 3,
                        User = _context.Users.SingleOrDefault(u => u.Id == 1),
                        Rating = 5,
                        Text = "It is always a good option for a healthy meal!",
                        Date = DateTime.Now,
                    },
                    new Comment
                    {
                        ProductId = 4,
                        User = _context.Users.SingleOrDefault(u => u.Id == 2),
                        Rating = 5,
                        Text = "Look! So yummy",
                        CommentImages = (new List<CommentImage>
                        {
                            new CommentImage
                            {
                                CommentId = 11,
                                Image = "./images/uploads/5cedc6e979941_5_Lychee.jpg"
                            },
                            new CommentImage
                            {
                                CommentId = 11,
                                Image = "./images/uploads/5cedc6e97b494_5_Lychee2.jpg"
                            }
                        }),
                        Date = DateTime.Now.AddMonths(-1).AddHours(6),
                    }
                });
                _context.SaveChanges();
            }
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<object>> GetComments(int id)
        {
            var response = new ResponseObject();

            try
            {
                if (id < 0)
                {
                    response.SetMessage("Inform a valid Product ID in order to retrieve comments");

                    return BadRequest(response);
                }

                var result = await _context.Comments.Where(c => c.Product.Id == id)
                    .Include(c => c.CommentImages).ToListAsync();

                if (result != null)
                {
                    IList<CommentResponse> commentResponses = result.ConvertAll(r =>
                                new CommentResponse { Id = r.Id, CommentImages = r.CommentImages, Date = r.Date, ProductId = r.ProductId, Rating = r.Rating, Text = r.Text });

                    response.SetResponse(commentResponses);
                    return Ok(response);
                }
                else
                {
                    response.SetMessage("There is no comments to this product.");
                    return Ok(response);
                }
            }
            catch (Exception ex)
            {
                _log.LogError(ex, "Error in CommentsController");
                response.SetMessage("Ops. We got a bad behaviour from our server. Try again later.");
                return StatusCode((int)HttpStatusCode.InternalServerError, response);
            }
        }

        [HttpPost]
        public async Task<object> Post([FromForm] CommentRequest comment)
        {
            var response = new ResponseObject();

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    var commentValidationString = comment.ValidateInsertComment();

                    if (!string.IsNullOrEmpty(commentValidationString))
                    {
                        response.SetMessage(commentValidationString);
                        return BadRequest(response);
                    }

                    if (!await _session.IsUserSignedIn())
                    {
                        response.SetMessage("User is not signed in");
                        return BadRequest(response);
                    }

                    var userId = await _session.GetIdFromSession();

                    //check if item exist in order_items table for that user
                    var order = await _context.Orders
                        .Include(o => o.OrderItems)
                        .FirstOrDefaultAsync(o => o.UserId == userId);

                    var orderItems = order.OrderItems.Where(oi => oi.ProductId == comment.ProductId).ToList();

                    if (orderItems != null && orderItems.Count > 0)
                    {
                        //checking some product comment is already added by that user
                        var comments = await _context.Comments.SingleOrDefaultAsync
                            (c => c.ProductId == comment.ProductId && c.UserId == userId);

                        if (comments != null)
                        {
                            response.SetMessage("You have already done a comment for the product.");
                            return BadRequest(response);
                        }

                        var newComment = new Comment
                        {
                            ProductId = comment.ProductId,
                            UserId = userId,
                            Rating = comment.Rating,
                            Text = comment.Text,
                            Date = DateTime.Now
                        };

                        _context.Comments.Add(newComment);
                        await _context.SaveChangesAsync();

                        if (comment.Images != null && comment.Images.Count > 0)
                        {
                            var newCommentImages = new List<CommentImage>();

                            foreach (var image in comment.Images)
                            {
                                var fileName = $"{Guid.NewGuid()}_{Path.GetFileName(image.FileName)}";
                                var filePath = Path.Combine(Directory.GetCurrentDirectory(), "images\\uploads\\", fileName);

                                using (var fileSteam = new FileStream(filePath, FileMode.Create))
                                {
                                    await image.CopyToAsync(fileSteam);
                                }

                                newCommentImages.Add(new CommentImage
                                {
                                    CommentId = newComment.Id,
                                    Image = $".\\images\\uploads\\{fileName}"
                                });
                            }

                            _context.CommentImages.AddRange(newCommentImages);

                            await _context.SaveChangesAsync();
                        }

                        transaction.Commit();

                        //returning all the comments to the product
                        var result = await _context.Comments.ToListAsync();
                        result = result.Where(c => c.ProductId == comment.ProductId).ToList();

                        if (result.Count > 0)
                        {
                            IList<CommentResponse> commentResponses = result.ConvertAll(r => 
                                new CommentResponse { Id = r.Id, CommentImages = r.CommentImages, Date = r.Date, ProductId = r.ProductId, Rating = r.Rating, Text = r.Text });

                            response.SetResponse(commentResponses);
                            return Ok(response);
                        }
                        else
                        {
                            response.SetMessage("There is no comments to this product.");
                            return Ok(response);
                        }
                    }
                    else
                    {
                        response.SetMessage("You have to buy the product before comment!");
                        return BadRequest(response);
                    }
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    _log.LogError(ex, "Error in CommentsController");
                    response.SetMessage("Ops. We got a bad behaviour from our server. Try again later.");
                    return StatusCode((int)HttpStatusCode.InternalServerError, response);
                }
            }
        }
    }
}
