﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using eCommerceCore.ApiSecurity.Interfaces;
using eCommerceCore.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace eCommerceCore.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CartController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly IApiSession _session;
        private readonly ILogger<CartController> _log;

        public CartController(AppDbContext appDbContext, IApiSession session, ILogger<CartController> log)
        {
            _context = appDbContext;
            _session = session;
            _log = log;

            if (_context.Carts.Count() == 0)
            {
                _context.Carts.AddRange(new List<Cart>
                {
                    new Cart {
                       User = _context.Users.SingleOrDefault(u => u.Id == 4),
                    },
                    new Cart {
                        User = _context.Users.SingleOrDefault(u => u.Id == 3),
                    }
                });
                _context.SaveChanges();
            }

            if (_context.CartItems.Count() == 0)
            {
                _context.CartItems.AddRange(new List<CartItem>
                {
                    new CartItem {
                        CartId = 1,
                        Product = _context.Products.SingleOrDefault(u => u.Id == 1),
                        Quantity = 1
                    },
                    new CartItem {
                        CartId = 1,
                        Product = _context.Products.SingleOrDefault(u => u.Id == 2),
                        Quantity = 4
                    },
                    new CartItem {
                        CartId = 2,
                        Product = _context.Products.SingleOrDefault(u => u.Id == 4),
                        Quantity = 3
                    }
                });
                _context.SaveChanges();
            }
        }
        

        [HttpGet]
        public async Task<object> GetCart()
        {
            var response = new ResponseObject();
            if (!await _session.IsUserSignedIn())
            {
                response.SetMessage("User is not signed in");
                return BadRequest(response);
            }

            var userId = await _session.GetIdFromSession();

            try
            {
                return await GetCartSumary();
            }
            catch (Exception ex)
            {
                _log.LogError(ex, "Error in ProductController");
                response.SetMessage("Ops. We got a bad behaviour from our server. Try again later.");
                return StatusCode((int)HttpStatusCode.InternalServerError, response);
            }
        }

        [HttpPost]
        public async Task<object> Post([FromBody] CartItem cartItem)
        {
            var response = new ResponseObject();
            if (!await _session.IsUserSignedIn())
            {
                response.SetMessage("User is not signed in");
                return BadRequest(response);
            }

            var userId = await _session.GetIdFromSession();

            //validate quantity
            var quantityValidationString = cartItem.IsValidQuantity();
            if (!string.IsNullOrEmpty(quantityValidationString))
            {
                response.SetMessage(quantityValidationString);
                return BadRequest(response);
            }

            //validate productID
            var product = await _context.Products.SingleOrDefaultAsync(p => p.Id == cartItem.ProductId);
            if (product == null)
            {
                response.SetMessage("Invalid product's identification");
                return BadRequest(response);
            }

            try
            {
                //check if user already have cartId
                var result = await _context.Carts.SingleOrDefaultAsync(p =>p.User.Id == userId);
                if (result == null)
                {
                    var cart = new Cart {
                        User = _context.Users.SingleOrDefault(p => p.Id == userId)
                    };
                    await _context.Carts.AddAsync(cart);
                    await _context.SaveChangesAsync();

                    var cartItm = new CartItem
                    {
                        CartId = cart.Id,
                        Product = _context.Products.SingleOrDefault(p => p.Id == cartItem.ProductId),
                        Quantity = cartItem.Quantity
                    };
                    await _context.CartItems.AddAsync(cartItm);
                    await _context.SaveChangesAsync();
                    response.SetMessage("Added to cart");
                }
                else
                {
                    var productExist = await _context.CartItems.SingleOrDefaultAsync(p => p.Product.Id == cartItem.ProductId && p.CartId == result.Id);
                    if (productExist != null)
                    {
                        response.SetMessage("Product already exists in your's cart");
                        return BadRequest(response);
                    }
                    else
                    {
                        var cartItm = new CartItem
                        {
                            CartId = result.Id,
                            Product = _context.Products.SingleOrDefault(p => p.Id == cartItem.ProductId),
                            Quantity = cartItem.Quantity
                        };
                        await _context.CartItems.AddAsync(cartItm);
                        await _context.SaveChangesAsync();
                        response.SetMessage("Added to cart");
                    }                  
                }
                return Ok(response);
            }
            catch (Exception ex)
            {
                _log.LogError(ex, "Error in UserController");
                response.SetMessage("Ops. We got a bad behaviour from our server. Try again later.");
                return StatusCode((int)HttpStatusCode.InternalServerError, response);
            }
        }

        [HttpPut]
        public async Task<Object> Put([FromBody] CartItem cartItem)
        {
            var response = new ResponseObject();
            if (!await _session.IsUserSignedIn())
            {
                response.SetMessage("User is not signed in");
                return BadRequest(response);
            }

            var userId = await _session.GetIdFromSession();

            //validate input
            var quantityValidationString = cartItem.IsValidQuantity();
            if (!string.IsNullOrEmpty(quantityValidationString))
            {
                response.SetMessage(quantityValidationString);
                return BadRequest(response);
            }

            try
            {
                var cart = await _context.Carts.SingleOrDefaultAsync(p => p.User.Id == userId);
                if (cart == null)
                {
                    response.SetMessage("Invalid shopping cart");
                    return BadRequest(response);
                }
                else
                {
                    var oldProduct = await _context.CartItems.SingleOrDefaultAsync(p => p.Product.Id == cartItem.ProductId && p.CartId == cart.Id);
                    if (oldProduct != null)
                    {
                        oldProduct.Quantity = cartItem.Quantity;
                        await _context.SaveChangesAsync();
                    }
                    else
                    {
                        response.SetMessage("Invalid product's identification");
                        return BadRequest(response);
                    }
                    var result = await _context.Carts.Include(o => o.CartItems).SingleOrDefaultAsync(p => p.User.Id == userId);
                    result.CartItems = await _context.CartItems.Include(o => o.Product).Where(p => p.CartId == result.Id).ToListAsync();

                    return await GetCartSumary();
                }
            }
            catch (Exception ex)
            {
                _log.LogError(ex, "Error in OrderController");
                response.SetMessage("Ops. We got a bad behaviour from our server. Try again later.");
                return StatusCode((int)HttpStatusCode.InternalServerError, response);
            }
        }

        public async Task<object> GetCartSumary()
        {
            var response = new ResponseObject();
            var userId = await _session.GetIdFromSession();

            try
            {
                var result = await _context.Carts.Include(o => o.CartItems).SingleOrDefaultAsync(p => p.User.Id == userId);
                result.CartItems = await _context.CartItems.Include(o => o.Product).Where(p => p.CartId == result.Id).ToListAsync();
                if (result.CartItems.Count > 0)
                {
                    var shippingCost = result.CartItems.Sum(ci => ci.Product.ShippingCost * ci.Quantity);
                    var subTotal = result.CartItems.Sum(ci => ci.Product.Price * ci.Quantity);
                    var tax = decimal.Round(subTotal * 0.13m, 2);
                    var total = decimal.Round(subTotal + shippingCost + tax, 2);

                    var cartItems = new List<Object>();
                    foreach (CartItem cartItm in result.CartItems)
                    {
                        var item = new
                        {
                            cartItm.Product.Id,
                            cartItm.Product.Name,
                            cartItm.Product.Price,
                            cartItm.Product.ShippingCost,
                            cartItm.Product.Image,
                            cartItm.Quantity
                        };
                        cartItems.Add(item);
                    }
                    response.SetResponse(new
                    {
                        result.Id,
                        subTotal,
                        tax,
                        shippingCost,
                        total,
                        cartItems
                    });
                }
                else
                {
                    response.SetMessage("There is no product to be shown.");
                }
                return Ok(response);
            }
            catch (Exception ex)
            {
                _log.LogError(ex, "Error in ProductController");
                response.SetMessage("Ops. We got a bad behaviour from our server. Try again later.");
                return StatusCode((int)HttpStatusCode.InternalServerError, response);
            }
        }

        [HttpDelete]
        public async Task<Object> Delete([FromBody] CartItem cartItem)
        {
            var response = new ResponseObject();
            if (!await _session.IsUserSignedIn())
            {
                response.SetMessage("User is not signed in");
                return BadRequest(response);
            }

            var userId = await _session.GetIdFromSession();

            try
            {
                var cart = await _context.Carts.SingleOrDefaultAsync(p => p.User.Id == userId);
                if (cart == null)
                {
                    response.SetMessage("Invalid shopping cart");
                    return BadRequest(response);
                }
                else
                {
                    var oldProduct = await _context.CartItems.SingleOrDefaultAsync(p => p.Product.Id == cartItem.ProductId && p.CartId == cart.Id);
                    if (oldProduct != null)
                    {
                        _context.CartItems.Remove(oldProduct);
                        await _context.SaveChangesAsync();
                    }
                    else
                    {
                        response.SetMessage("Invalid product's identification");
                        return BadRequest(response);
                    }
                    return await GetCartSumary();
                }
            }
            catch (Exception ex)
            {
                _log.LogError(ex, "Error in OrderController");
                response.SetMessage("Ops. We got a bad behaviour from our server. Try again later.");
                return StatusCode((int)HttpStatusCode.InternalServerError, response);
            }
        }
    }
}
