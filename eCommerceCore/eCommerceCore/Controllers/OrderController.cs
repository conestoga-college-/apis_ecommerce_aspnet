﻿using eCommerceCore.ApiSecurity.Interfaces;
using eCommerceCore.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace eCommerceCore.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class OrderController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly IApiSession _session;
        private readonly ILogger<OrderController> _log;

        public OrderController(AppDbContext appDbContext, IApiSession session, ILogger<OrderController> log)
        {
            _context = appDbContext;
            _session = session;
            _log = log;

            if (_context.Orders.Count() == 0)
            {
                _context.Orders.AddRange(new List<Order>
                {
                    new Order {
                       UserId = 3,
                       OrderDate = DateTime.Now,
                       SubTotal = 1.3m,
                       OrderNumber = Guid.NewGuid().ToString(),
                       Tax = 0.7m,
                       ShippingCost = 2.1m,
                       Total = 2.4m,
                       Status = OrderStatus.Submitted
                    },
                    new Order {
                       UserId = 2,
                       OrderDate = DateTime.Now,
                       SubTotal = 1.2m,
                       OrderNumber = Guid.NewGuid().ToString(),
                       Tax = 2,
                       ShippingCost = 2,
                       Total = 2,
                       Status = OrderStatus.Submitted
                    }            
                });
                _context.SaveChanges();
            }

            if (_context.OrderItems.Count() == 0)
            {
                _context.OrderItems.AddRange(new List<OrderItem>
                {
                    new OrderItem {
                       OrderId = 1,
                       ProductId = 1,
                       ProductName = "Bananas",
                       ProductImage = "./images/products/bananas.jpg",
                       ProductDescription = "Soft, sweet and delicious, the banana is a popular choice for breakfast, snacks or " +
                    "any time of the day. The vibrant yellow peel provides natural protection while storing, and is a " +
                    "great indicator of when the fruit is ready to eat!",
                       ProductPrice = 0.15m,
                       ProductShippingCost = 0.05m,
                       ProductQuantity = 2
                    },
                    new OrderItem {
                       OrderId = 1,
                       ProductId = 3,
                       ProductName = "Brussels Sprouts",
                       ProductImage = "./images/products/brussels_sprouts.jpg",
                       ProductDescription = "Enjoy Brussel sprouts roasted, steamed, boiled, or grilled. Excellent source of vitamin" +
                    " K and high in fibre. Try this vegetable for your next side dish.",
                       ProductPrice = 3.30m,
                       ProductShippingCost = 1.00m,
                       ProductQuantity = 1
                    }
                });
                _context.SaveChanges();
            }
        }

        [HttpPost("finalizeorder")]
        public async Task<object> Post()
        {
            var response = new ResponseObject();

            using (var transaction = _context.Database.BeginTransaction())
            {
                try
                {
                    if (!await _session.IsUserSignedIn())
                    {
                        response.SetMessage("User is not signed in");
                        return BadRequest(response);
                    }

                    var userId = await _session.GetIdFromSession();

                    var cart = await _context.Carts
                        .Include(c => c.CartItems)
                        .Include(c => c.User)
                        .Include("CartItems.Product")
                        .FirstOrDefaultAsync(c => c.User.Id == userId);

                    if(cart != null && cart.CartItems.Count == 0)
                    {
                        response.SetMessage("There are no products in the shopping cart.");
                        return BadRequest(response);
                    }

                    var shippingCost = cart.CartItems.Sum(ci => ci.Product.ShippingCost * ci.Quantity);
                    var subTotal = cart.CartItems.Sum(ci => ci.Product.Price * ci.Quantity);
                    var tax = decimal.Round(subTotal * 0.13m, 2);
                    var total = decimal.Round(subTotal + shippingCost + tax, 2);

                    var orderItems = new List<OrderItem>();

                    var order = new Order
                    {
                        OrderDate = DateTime.Now,
                        ShippingCost = shippingCost,
                        Status = OrderStatus.Submitted,
                        SubTotal = subTotal,
                        Tax = tax,
                        Total = total,
                        UserId = cart.User.Id
                    };

                    order.GenerateOrderNumber();

                    _context.Orders.Add(order);
                    await _context.SaveChangesAsync();

                    foreach (var ci in cart.CartItems)
                    {
                        orderItems.Add(new OrderItem
                        {
                            OrderId = order.Id,
                            ProductId = ci.Product.Id,
                            ProductDescription = ci.Product.Description,
                            ProductImage = ci.Product.Image,
                            ProductName = ci.Product.Name,
                            ProductPrice = ci.Product.Price,
                            ProductQuantity = ci.Quantity,
                            ProductShippingCost = ci.Product.ShippingCost
                        });
                    }

                    _context.OrderItems.AddRange(orderItems);

                    _context.CartItems.RemoveRange(cart.CartItems);

                    await _context.SaveChangesAsync();

                    transaction.Commit();

                    response.SetResponse(new
                    {
                        order.OrderNumber,
                        OrderDate = new DateTime(order.OrderDate.Year, order.OrderDate.Month, order.OrderDate.Day, order.OrderDate.Hour, order.OrderDate.Minute, order.OrderDate.Second),
                        Status = (int)order.Status,
                        order.ShippingCost,
                        order.Total
                    });

                    return Ok(response);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();
                    _log.LogError(ex, "Error in OrderController");
                    response.SetMessage("Ops. We got a bad behaviour from our server. Try again later.");
                    return StatusCode((int)HttpStatusCode.InternalServerError, response);
                }
            }
        }

        [HttpGet]
        public async Task<object> GetOrders()
        {
            var response = new ResponseObject();
            if (!await _session.IsUserSignedIn())
            {
                response.SetMessage("User is not signed in");
                return BadRequest(response);
            }

            var userId = await _session.GetIdFromSession();

            try
            {
                var result = await _context.Orders.Where(p => p.UserId == userId).ToListAsync();

                if (result.Count > 0)
                {
                    var orders = new List<Object>();
                    foreach (Order order in result)
                    {
                        var orderDetail = new
                        {
                            order.Id,
                            order.OrderDate,
                            order.OrderNumber,
                            order.SubTotal,
                            order.Tax,
                            order.ShippingCost,
                            order.Total,
                            order.Status
                        };

                        orders.Add(orderDetail);
                    }

                    response.SetResponse(orders);
                    return Ok(response);
                }
                else
                {
                    response.SetMessage("There is no order to be shown.");
                    return BadRequest(response);
                }
            }
            catch (Exception ex)
            {
                _log.LogError(ex, "Error in ProductController");
                response.SetMessage("Ops. We got a bad behaviour from our server. Try again later.");
                return StatusCode((int)HttpStatusCode.InternalServerError, response);
            }
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<object>> GetOrder(int id)
        {
            var response = new ResponseObject();

            if (!await _session.IsUserSignedIn())
            {
                response.SetMessage("User is not signed in");
                return BadRequest(response);
            }

            var userId = await _session.GetIdFromSession();

            try
            {
                var result = await _context.Orders.Include(o => o.OrderItems).SingleOrDefaultAsync(p => p.Id == id && p.UserId == userId);

                if (result != null)
                {
                    var orderItems = new List<Object>();
                    foreach (OrderItem orderItm in result.OrderItems)
                    {
                        var orderItem = new
                        {
                            orderItm.Id,
                            orderItm.ProductName,
                            orderItm.ProductPrice,
                            orderItm.ProductShippingCost,
                            orderItm.ProductQuantity,
                            orderItm.ProductImage
                        };
                        orderItems.Add(orderItem);
                    }

                    var order = new
                    {
                        result.Id,
                        result.OrderDate,
                        result.OrderNumber,
                        result.SubTotal,
                        result.Tax,
                        result.ShippingCost,
                        result.Total,
                        result.Status,
                        orderItems
                    };

                    response.SetResponse(order);
                    return Ok(response);
                }

                else
                {
                    response.SetMessage("There is no order to be shown.");
                    return BadRequest(response);
                }
            }
            catch (Exception ex)
            {
                _log.LogError(ex, "Error in OrderController");
                response.SetMessage("Ops. We got a bad behaviour from our server. Try again later.");
                return StatusCode((int)HttpStatusCode.InternalServerError, response);
            }
        }
    }
}
