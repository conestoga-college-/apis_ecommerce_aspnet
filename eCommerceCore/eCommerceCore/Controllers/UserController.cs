﻿using eCommerceCore.ApiSecurity;
using eCommerceCore.ApiSecurity.Interfaces;
using eCommerceCore.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace eCommerceCore.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly IApiSession _session;
        private readonly ILogger<UserController> _log;

        public UserController(AppDbContext appDbContext, IApiSession session, ILogger<UserController> log)
        {
            _context = appDbContext;
            _session = session;
            _log = log;

            if (_context.Users.Count() == 0)
            {
                _context.Users.AddRange(new List<User>
                {
                    new User {
                    Email = "thao@b.com",
                    Password = Security.HashPassword("Thao123"),
                    FirstName = "Thao",
                    LastName = "Thi",
                    Username = "thaothi",
                    ShippingAddress = "Shipping Address 1 Thao"
                },
                    new User {
                    Email = "rafael@b.com",
                    Password = Security.HashPassword("Rafael123"),
                    FirstName = "Rafael",
                    LastName = "Meneghelo",
                    Username = "rmeneghelo",
                    ShippingAddress = "Shipping Address 1 Rafael"
                },
                    new User {
                    Email = "roberta@b.com",
                    Password = Security.HashPassword("Roberta123"),
                    FirstName = "Roberta",
                    LastName = "Martins",
                    Username = "robeape",
                    ShippingAddress = "Shipping Address 1 Roberta"
                }
                });

                _context.SaveChanges();
            }
        }

        [HttpPost]
        public async Task<object> Post([FromBody] User user)
        {
            var response = new ResponseObject();

            try
            {
                var userValidationString = user.IsValidUserCreate();
                if (!string.IsNullOrEmpty(userValidationString))
                {
                    response.SetMessage(userValidationString);

                    return BadRequest(response);
                }

                var registeredUser = await _context.Users.FirstOrDefaultAsync(u => (u.Username == user.Username) || (u.Email == user.Email));

                if(registeredUser != null)
                {
                    response.SetMessage("Please choose another email or username for registration.");

                    return BadRequest(response);
                }

                var newUser = new User
                {
                    Email = user.Email,
                    Password = Security.HashPassword(user.Password),
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Username = user.Username,
                    ShippingAddress = user.ShippingAddress
                };

                await _context.Users.AddAsync(newUser);

                await _context.SaveChangesAsync();

                _session.SetIdToSession(newUser.Id);

                response.SetMessage("User created.");

                return Ok(response);
            }
            catch (Exception ex)
            {
                _log.LogError(ex, "Error in UserController");
                response.SetMessage("Ops. We got a bad behaviour from our server. Try again later.");
                return StatusCode((int)HttpStatusCode.InternalServerError, response);
            }
        }

        [HttpPut]
        public async Task<object> Put([FromBody] User user)
        {
            var response = new ResponseObject();

            try
            {
                if(!await _session.IsUserSignedIn())
                {
                    response.SetMessage("User is not signed in");
                    return BadRequest(response);
                }

                user.Id = await _session.GetIdFromSession();

                var userValidationString = user.IsValidUserUpdate();
                if (!string.IsNullOrEmpty(userValidationString))
                {
                    response.SetMessage(userValidationString);

                    return BadRequest(response);
                }

                var checkUserDb = await _context.Users.FirstOrDefaultAsync(u => (u.Email == user.Email) && (u.Id != user.Id));

                if (checkUserDb != null)
                {
                    response.SetMessage("Email is already in use. Please choose another one.");

                    return BadRequest(response);
                }

                var registeredUser = await _context.Users.FirstOrDefaultAsync(u => u.Id == user.Id);

                registeredUser.Email = user.Email;
                registeredUser.FirstName = user.FirstName;
                registeredUser.LastName = user.LastName;
                registeredUser.ShippingAddress = user.ShippingAddress;

                await _context.SaveChangesAsync();

                response.SetMessage("User updated.");

                return Ok(response);
            }
            catch (Exception ex)
            {
                _log.LogError(ex, "Error in UserController");
                response.SetMessage("Ops. We got a bad behaviour from our server. Try again later.");
                return StatusCode((int)HttpStatusCode.InternalServerError, response);
            }
        }

        [HttpPost("changepassword")]
        public async Task<object> ChangePassword([FromBody] JObject request)
        {
            var response = new ResponseObject();
            if (!await _session.IsUserSignedIn())
            {
                response.SetMessage("User is not signed in");
                return BadRequest(response);
            }

            var userId = await _session.GetIdFromSession();
            
            try
            {
                var registeredUser = await _context.Users.FirstOrDefaultAsync(u => u.Id == userId);

                if (registeredUser != null)
                {
                    if (Security.VerifyHashedPassword(registeredUser.Password, request.GetValue("old_password").ToString()))
                    {
                        string newPassword = request.GetValue("new_password").ToString();
                        var userValidationString = registeredUser.IsValidPassword(newPassword);
                        if (!string.IsNullOrEmpty(userValidationString))
                        {
                            response.SetMessage(userValidationString);
                            return BadRequest(response);
                        }
                        else
                        {
                            registeredUser.Password = Security.HashPassword(newPassword);
                            await _context.SaveChangesAsync();
                        }
                    }
                    else
                    {
                        response.SetMessage("Wrong password. Please try again.");
                        return BadRequest(response);
                    }
                }
                else
                {
                    response.SetMessage("Wrong user's identification. Please try again.");
                    return BadRequest(response);
                }

                response.SetMessage("Your password has changed successfully!");
                return Ok(response);
            }
            catch (Exception ex)
            {
                _log.LogError(ex, "Error in UserController");
                response.SetMessage("Ops. We got a bad behaviour from our server. Try again later.");
                return StatusCode((int)HttpStatusCode.InternalServerError, response);
            }
        }
    }
}
