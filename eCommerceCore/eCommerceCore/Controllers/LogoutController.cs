﻿using eCommerceCore.ApiSecurity.Interfaces;
using eCommerceCore.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace eCommerceCore.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class LogoutController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly IApiSession _session;
        private readonly ILogger<LoginController> _log;

        public LogoutController(AppDbContext appDbContext, IApiSession session, ILogger<LoginController> log)
        {
            _context = appDbContext;
            _session = session;
            _log = log;
        }

        [HttpPost]
        public async void Post()
        {
            if (await _session.IsUserSignedIn())
            {
                _session.DestroySession();
            }
        }
    }
}
