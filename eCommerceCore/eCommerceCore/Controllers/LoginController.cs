﻿using eCommerceCore.ApiSecurity;
using eCommerceCore.ApiSecurity.Interfaces;
using eCommerceCore.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Net;
using System.Threading.Tasks;

namespace eCommerceCore.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class LoginController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly IApiSession _session;
        private readonly ILogger<LoginController> _log;

        public LoginController(AppDbContext appDbContext, IApiSession session, ILogger<LoginController> log)
        {
            _context = appDbContext;
            _session = session;
            _log = log;
        }


        [HttpPost]
        public async Task<object> Post([FromBody] User user)
        {
            var response = new ResponseObject();
            try
            {
                var hashedPassword = Security.HashPassword(user.Password);

                var result = await _context.Users.FirstOrDefaultAsync(u => u.Username == user.Username);

                if (result != null && Security.VerifyHashedPassword(result.Password, user.Password))
                {
                    _session.SetIdToSession(result.Id);

                    var customerId = await _session.GetIdFromSession();

                    response.SetResponse(new
                    {
                        result.Email,
                        result.FirstName,
                        result.LastName,
                        result.Username,
                        result.ShippingAddress
                    });

                    return Ok(response);
                }
                else
                {
                    response.SetMessage("Wrong username or password. Please try again.");
                    return BadRequest(response);
                }
            }
            catch (Exception ex)
            {
                _log.LogError(ex, "Error in LoginController");
                response.SetMessage("Ops. We got a bad behaviour from our server. Try again later.");
                return StatusCode((int)HttpStatusCode.InternalServerError, response);
            }
            
        }
    }
}
