﻿using System.ComponentModel.DataAnnotations.Schema;

namespace eCommerceCore.Models
{
    [Table("CartItems")]
    public class CartItem
    {
        public int Id { get; set; }

        [ForeignKey("CartId")]
        public int CartId { get; set; }

        [ForeignKey("ProductId")]
        public int ProductId { get; set; }
        public Product Product { get; set; }
        
        public int Quantity { get; set; }

        public string IsValidQuantity()
        {
            var result = string.Empty;
            if (Quantity <= 0)
            {
                result = "Invalid quantity of the product";
            }
            return result.Trim();
        }
    }
}
