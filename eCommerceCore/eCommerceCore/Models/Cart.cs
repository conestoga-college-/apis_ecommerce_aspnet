﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace eCommerceCore.Models
{
    [Table("Carts")]
    public class Cart
    {
        public int Id { get; set; }

        [ForeignKey("UserId")]
        public User User { get; set; }

        public ICollection<CartItem> CartItems { get; set; }
    }
}
