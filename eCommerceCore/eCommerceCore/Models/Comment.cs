﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace eCommerceCore.Models
{
    [Table("Comments")]
    public class Comment
    {
        public int Id { get; set; }

        [ForeignKey("ProductId")]
        public int ProductId { get; set; }
        public Product Product { get; set; }

        [ForeignKey("UserId")]
        public int UserId { get; set; }
        public User User { get; set; }

        public int Rating { get; set; }
        public string Text { get; set; }
        public DateTime Date { get; set; }

        public IList<CommentImage> CommentImages { get; set; }
    }
}
