﻿using System.ComponentModel.DataAnnotations.Schema;

namespace eCommerceCore.Models
{
    [Table("Products")]
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public decimal Price { get; set; }
        public decimal ShippingCost { get; set; }
        public bool IsActive { get; set; }
    }
}
