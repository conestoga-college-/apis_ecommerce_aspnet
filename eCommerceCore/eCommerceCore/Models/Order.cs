﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace eCommerceCore.Models
{
    [Table("Orders")]
    public class Order
    {
        public int Id { get; set; }

        [ForeignKey("UserId")]
        public int UserId { get; set; }

        public DateTime OrderDate { get; set; }
        public string OrderNumber { get; set; }
        public decimal SubTotal { get; set; }
        public decimal Tax { get; set; }
        public decimal ShippingCost { get; set; }
        public decimal Total { get; set; }
        public OrderStatus Status { get; set; }

        public IList<OrderItem> OrderItems { get; set; }

        public void GenerateOrderNumber()
        {
            OrderNumber = Guid.NewGuid().ToString();
        }
    }
}
