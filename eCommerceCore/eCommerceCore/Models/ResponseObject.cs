﻿namespace eCommerceCore.Models
{
    public class ResponseObject
    {
        public object Response { get; private set; }
        public string Message { get; private set; }

        public void SetResponse(object response)
        {
            Message = null;
            this.Response = response;
        }

        public void SetMessage(string message)
        {
            Response = null;
            this.Message = message;
        }
    }
}
