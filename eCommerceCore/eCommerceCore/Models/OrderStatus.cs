﻿namespace eCommerceCore.Models
{
    public enum OrderStatus
    {
        Submitted,
        PaymentConfirmed,
        ReadyToSend,
        OutToDelivery,
        Delivered
    }
}
