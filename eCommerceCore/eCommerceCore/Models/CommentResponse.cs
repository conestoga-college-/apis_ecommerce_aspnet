﻿using System;
using System.Collections.Generic;

namespace eCommerceCore.Models
{
    public class CommentResponse
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public int Rating { get; set; }
        public string Text { get; set; }
        public DateTime Date { get; set; }
        public IList<CommentImage> CommentImages { get; set; }
    }
}
