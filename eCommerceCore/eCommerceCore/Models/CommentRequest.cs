﻿using Microsoft.AspNetCore.Http;
using System.Collections.Generic;

namespace eCommerceCore.Models
{
    public class CommentRequest
    {
        private readonly IList<string> allowedContentTypes = new List<string> { "image/jpeg", "image/jpg", "image/png" };

        public int ProductId { get; set; }
        public int Rating { get; set; }
        public string Text { get; set; }
        public IList<IFormFile> Images { get; set; }

        public string ValidateInsertComment()
        {
            var result = string.Empty;

            if (Rating < 0 || Rating > 5)
            {
                result += "Rating must be between 0 and 5. ";
            }

            if (ProductId < 1)
            {
                result += "It is not a valid Product ID. ";
            }

            if (Images != null && Images.Count > 0)
            {
                bool valid = true;
                foreach (var image in Images)
                {
                    if (!allowedContentTypes.Contains(image.ContentType))
                    {
                        valid = false;
                    }
                }

                if (!valid)
                {
                    result += "We only accept files with the extensions: jpeg, jpg, png";
                }
            }

            return result.Trim();
        }
    }
}
