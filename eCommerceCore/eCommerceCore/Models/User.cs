﻿using System.ComponentModel.DataAnnotations.Schema;

namespace eCommerceCore.Models
{
    [Table("Users")]
    public class User
    {
        public int Id { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string ShippingAddress { get; set; }

        public bool IsValidUserLogin()
        {
            if(string.IsNullOrEmpty(Username) || string.IsNullOrEmpty(Password))
            {
                return false;
            }

            return true;
        }

        public string IsValidUserCreate()
        {
            var result = string.Empty;

            if (string.IsNullOrEmpty(Email) || !Commons.Commons.IsValidEmail(Email))
            {
                result += "Email is empty or in a wrong format. ";
            }

            if (string.IsNullOrEmpty(Password))
            {
                result += "Password is empty. ";
            }

            if (!Commons.Commons.IsValidPassword(Password))
            {
                result += "Password does not contain lower case letter, upper case letter and digit. ";
            }

            if (Password.Length < 6 || Password.Length > 16)
            {
                result += "Password must be between 6 and 16 characters. ";
            }

            if(string.IsNullOrEmpty(FirstName))
            {
                result += "First name is empty. ";
            }

            if (string.IsNullOrEmpty(LastName))
            {
                result += "Last Name is empty. ";
            }

            if(string.IsNullOrEmpty(Username))
            {
                result += "Username is empty. ";
            }

            if(string.IsNullOrEmpty(ShippingAddress))
            {
                result += "Shipping address is empty. ";
            }

            return result.Trim();
        }

        public string IsValidUserUpdate()
        {
            var result = string.Empty;

            if (string.IsNullOrEmpty(Email) || !Commons.Commons.IsValidEmail(Email))
            {
                result = "Email is empty or in a wrong format. ";
            }

            if (string.IsNullOrEmpty(FirstName))
            {
                result += "First name is empty. ";
            }

            if (string.IsNullOrEmpty(LastName))
            {
                result += "Last Name is empty. ";
            }

            if (string.IsNullOrEmpty(ShippingAddress))
            {
                result += "Shipping address is empty. ";
            }

            return result.Trim();
        }

        public string IsValidPassword(string password)
        {
            var result = string.Empty;
            if (string.IsNullOrEmpty(password))
            {
                result += "Password is empty. ";
            }

            if (!Commons.Commons.IsValidPassword(password))
            {
                result += "Password does not contain lower case letter, upper case letter and digit. ";
            }

            if (password.Length < 6 || password.Length > 16)
            {
                result += "Password must be between 6 and 16 characters. ";
            }

            return result.Trim();
        }
    }
}
