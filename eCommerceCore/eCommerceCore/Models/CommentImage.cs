﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.IO;

namespace eCommerceCore.Models
{
    [Table("CommentImages")]
    public class CommentImage
    {
        public int Id { get; set; }

        [ForeignKey("CommentId")]
        public int CommentId { get; set; }

        public string Image { get; set; }

        public bool ValidateImageExtension()
        {
            var validExtensions = new List<string> { "jpeg", "jpg", "png" };
            string extension = Path.GetExtension(Image);

            var valid = validExtensions.TrueForAll(n => n == extension);
            return valid;
        }

    }
}
