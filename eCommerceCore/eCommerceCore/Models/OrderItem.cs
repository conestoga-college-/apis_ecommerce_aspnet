﻿using System.ComponentModel.DataAnnotations.Schema;

namespace eCommerceCore.Models
{
    [Table("OrderItems")]
    public class OrderItem
    {
        public int Id { get; set; }

        [ForeignKey("OrderId")]
        public int OrderId { get; set; }

        public int ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductDescription { get; set; }
        public string ProductImage { get; set; }
        public decimal ProductPrice { get; set; }
        public decimal ProductShippingCost { get; set; }
        public decimal ProductQuantity { get; set; }
    }
}
