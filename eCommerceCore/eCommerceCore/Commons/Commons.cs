﻿using System;
using System.Net.Mail;
using System.Text.RegularExpressions;

namespace eCommerceCore.Commons
{
    public static class Commons
    {
        public static bool IsValidEmail(string email)
        {
            try
            {
                var emailCheck = new MailAddress(email);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }

        public static bool IsValidPassword(string password)
        {
            return !Regex.IsMatch(password, "[a-z]+") || !Regex.IsMatch(password, @"[A-Z]+") || !Regex.IsMatch(password, @"[0-9]+") ? false : true;
        }
    }
}
