# APIs_eCommerce_ASPNet

APIs developed in C# (ASP .NET CORE) for an imaginary eCommerce

Technologies
Server ASP.NETCore 2.1
Client Postman
Database SQLServer/EFCore

Task
Working in groups of up to 3, create an ASP.NETCore implementation of the server components required to provide data from a SQLServer database instance (using Entity Framework Core) to the VueJS client described in Project 4.

To complete this project, you will need database entities like the following:

• Product (description, image, pricing, shipping cost)

• User (email, password, username, purchase history, shipping address)

• Comments (product, user, rating, image(s), text)

• Cart (products, quantities, user)